resource "kubernetes_deployment_v1" "frontend" {
  metadata {
    name = "frontend"
    labels = {
      test = "frontend"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "frontend"
      }
    }

    template {
      metadata {
        labels = {
          test = "frontend"
        }
      }

      spec {
        container {
          image = "493270667162.dkr.ecr.ap-southeast-1.amazonaws.com/ffm-front:latest"
          name  = "frontend"

          resources {
            limits = {
              cpu    = "1"
              memory = "2G"
            }
            requests = {
              cpu    = ".5m"
              memory = "512Mi"
            }
          }
        }
      }
    }
  }
}


resource "kubernetes_service_v1" "frontend_svc" {
  metadata {
    name = "frontend"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.frontend.metadata.0.labels.test
    }
    
    port {
      port        = 3000
      target_port = 3000
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress_v1" "ffm_ingress2" {
  metadata {
    name = "ffm-ingress2"
    annotations = {
        "kubernetes.io/ingress.class" = "alb"
        "alb.ingress.kubernetes.io/scheme" = "internet-facing"
        "alb.ingress.kubernetes.io/listen-ports" = "[{\"HTTP\": 80}]"
    }
  }

  spec {
    default_backend {
      service {
        name = "frontend"
        port {
          number = 3000
        }
      }
    }

    rule {
      http {
        path {
          path = "/api/v1/field-force/auth"
          path_type = "Prefix"
          backend {
            service {
              name = "auth"
              port {
                number = 8000
              }
            }
          }
        }
        path {
          path = "/api/v1/field-force/users"
          path_type = "Prefix"
          backend {
            service {
              name = "user"
              port {
                number = 3000
              }
            }
          }
        }
        path {
          path = "/api/v1/field-force/task"
          path_type = "Prefix"
          backend {
            service {
              name = "task"
              port {
                number = 8000
              }
            }
          }
        }
        path {
          path = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "frontend"
              port {
                number = 3000
              }
            }
          }
        }
      }
    }
  }
}